﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class GreeterImpl : Greeter.GreeterBase
    {
        public override Task<EmptyResponse> SayHello(HelloRequest request, ServerCallContext context)
        {
            Console.WriteLine("Hello " + request.Name);
            return Task.FromResult(new EmptyResponse { });
        }
    }
}
