﻿using Grpc.Core;
using System;

namespace Server
{
    class Program
    {
        const int Port = 8080;

        static void Main(string[] args)
        {

            Grpc.Core.Server server = new Grpc.Core.Server
            {
                Services = { Greeter.BindService(new GreeterImpl()) },
                Ports = { new ServerPort("localhost", Port, ServerCredentials.Insecure) }
            };
            server.Start();

            Console.WriteLine("Greeter server listening on port " + Port);
            Console.WriteLine("Press any key to stop the server");
            Console.WriteLine();
            Console.ReadKey();

            server.ShutdownAsync().Wait();

        }
    }
}
