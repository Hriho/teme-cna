﻿
using Grpc.Core;
using System;


namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Channel channel = new Channel("127.0.0.1:8080", ChannelCredentials.Insecure);
            var client = new Greeter.GreeterClient(channel);

            Console.WriteLine("Type exit if you want to close the application.");

            while (true)
            {
                Console.WriteLine("Enter your name: ");
                var name = Console.ReadLine();
                if (name.ToLower().Equals("exit")) break;
                client.SayHello(new HelloRequest { Name = name });
                Console.WriteLine();
            }

            channel.ShutdownAsync().Wait();
            Console.ReadKey();
        }
    }
}
