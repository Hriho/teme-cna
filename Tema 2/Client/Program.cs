﻿using Grpc.Core;
using System;
using System.Text.RegularExpressions;

namespace Client
{
    class Client
    {
        private const string DateRegex = @"\b(((0?[469]|11)/(0?[1-9]|[12]\d|30)|(0?[13578]|1[02])/(0?[1-9]|[12]\d|3[01])|0?2/(0?[1-9]|1\d|2[0-8]))/([1-9]\d{3}|\d{2})|0?2/29/([1-9]\d)?([02468][048]|[13579][26]))\b";
        private const int DateLength= 10;

        private static bool dateIsValid(string date)
        {
            var dateRegex = new Regex(DateRegex);
            return dateRegex.IsMatch(date);
        }

        static void Main(string[] args)
        {
            Channel channel = new Channel("127.0.0.1:8080", ChannelCredentials.Insecure);
            var client = new Generated.ZodiacService.ZodiacServiceClient(channel);

            Console.WriteLine("Type 'exit' if you want to close the application.");

            while (true)
            {
                Console.Write("[Date Format: mm/dd/yyyy] Enter the date: ");
                var date = Console.ReadLine();
                if (date.ToLower().Equals("exit")) break;
                if(dateIsValid(date) && date.Length == DateLength)
                {
                    var response = client.GetZodiacSign(new Generated.ZodiacRequest
                    {
                        Date = date
                    });
                    Console.WriteLine("The zodiac sign for the date: " + date + " is " + response.ZodiacSign);
                    Console.WriteLine();
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("Date is not valid ! Please follow the format [Date Format: mm/dd/yyyy] and be careful about leap years !");
                    Console.WriteLine();
                    Console.WriteLine();
                }
            }
            channel.ShutdownAsync().Wait();
            Console.ReadKey();
        }
    }
}