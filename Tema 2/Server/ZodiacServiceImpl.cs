﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class ZodiacServiceImpl : Generated.ZodiacService.ZodiacServiceBase
    {
        private List<ZodiacSign> zodiacSigns;

        public override Task<ZodiacResponse> GetZodiacSign(ZodiacRequest request, ServerCallContext context)
        {
            string zodiacSign = "ERROR";
            DateTime date = DateTime.ParseExact(request.Date, ZodiacSign.Format, System.Globalization.CultureInfo.InvariantCulture);

            foreach (ZodiacSign sign in zodiacSigns)
            {
                if(sign.isBetween(date))
                {
                    zodiacSign = sign.Sign;
                    break;
                }
            }

            return Task.FromResult(new ZodiacResponse { ZodiacSign =  zodiacSign });
        }

        public ZodiacServiceImpl(string filePath)
        {
            Console.WriteLine("[INFO] Reading data from file . . .");
            try {
                StreamReader reader = new StreamReader(filePath);
                string[] words = reader.ReadToEnd().Split(' ');

            zodiacSigns = new List<ZodiacSign>();

            const int wordsPerLine = 3;
            const int zodiacSignsNumber = 12;
            int index = 0;

            while(index < wordsPerLine * zodiacSignsNumber)
            {
                zodiacSigns.Add(new ZodiacSign(words[index], words[index + 1], words[index + 2]));
                index += 3;
            }
                Console.WriteLine("[INFO] Data has been read successfuly !");
            } 
            catch( Exception e)
            {
               Console.WriteLine("[ERROR] An error occured while reading from file! ");
               Console.WriteLine();
               Console.WriteLine("PATH: " + filePath);
               Console.WriteLine();
               Console.WriteLine("ERROR: " + e.ToString());
            }
        }
    }
}
