﻿using Grpc.Core;
using System;

namespace Server
{
    class Server
    {
        private const int Port = 8080;
        private const string filePath = "../../../ZodiacSigns.txt";

        static void Main(string[] args)
        {
            Grpc.Core.Server server = new Grpc.Core.Server
            {
                Services = { Generated.ZodiacService.BindService(new ZodiacServiceImpl(filePath)) },
                Ports = { new ServerPort("localhost", Port, ServerCredentials.Insecure) }
            };
            server.Start();

            Console.WriteLine("[INFO] Server listening on port " + Port);
            Console.WriteLine("Press any key to stop the server");
            Console.WriteLine();
            Console.ReadKey();

            server.ShutdownAsync().Wait();
        }
    }
}
