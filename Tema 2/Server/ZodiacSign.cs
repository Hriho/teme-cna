﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Server
{
    class ZodiacSign
    {
        public const string Format = "MM/dd/yyyy"; 

        private string sign;
        public string Sign
        {
            get { return sign; }  
            set { sign = value; }
        }

        private DateTime startDate;
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        private DateTime endDate;
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        public ZodiacSign(string sign, string startDate, string endDate)
        {
            this.sign = sign;
            this.startDate = DateTime.ParseExact(startDate, Format, System.Globalization.CultureInfo.InvariantCulture);
            this.endDate = DateTime.ParseExact(endDate, Format, System.Globalization.CultureInfo.InvariantCulture);
        }

        public bool isBetween(DateTime date)
        {
            int endYearGap = endDate.Month == 1 && date.Month == 12 && date.Day >= 22 ? 1 : 0;
            int startYearGap = startDate.Month == 12 && date.Month == 1 && date.Day <= 19 ? -1 : 0;
            if (startDate.AddYears(Math.Abs(date.Year - startDate.Year + startYearGap)) <= date && date <= endDate.AddYears(Math.Abs(date.Year - endDate.Year + endYearGap)))
            {
                return true;
            }
            return false;
        }
    }
}
